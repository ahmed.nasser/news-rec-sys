# Personalized News Recommendation: Methodological Approach (Social Data Science Master's Project)

In this repository, I am presenting my work on News recommendations using MIcrosoft News Data (**MIND**). I am comparing the performance of **Matrix Factorization (MF)** vs **Neural Collaborative Filtering (NCF)** using Multi-layer Perceptron (MLP).

## Directories structure


### results 
contains the generated logs while training methods in the pre and actual study.

### final models 
contains the resulting models from the final experiments.

### Data
Contains the datasets after being converted to a suitable format (see `News_Data_Preparation.ipynb`)


## Notebooks structure

#### `News_Data_Preparation.ipynb`


Convert the MIND-small dataset to the same format as in **NCF** and **MF** revisited papers to apply the same methods in the exact setup. After performing this step we will have the below files.

- _**news.test.negative**_ contains negative instances for evaluation.

- **_news.test.rating_**: contains testing positive pairs.

- **_news.train.rating_**: used for training the MF and MLP.

#### `Matrix_Factorization Experiment.ipynb`
This notebook shows the Matrix Factorization experiment.

#### `MLP - Neural collaborative Filtering Experiment.ipynb`
This notebook shows the Neural Collaborative Filtering experiment.

#### `Graphs Generation.ipynb`
This notebook is responsible for generating the graphs related to the pre and actual experiments based on the logs in the `results` directory. 


## .py files structure

#### `Dataset.py` : 
Data class for loading train/test data.  
#### `evaluate.py` : 
Evaluation logic e.g. @HR, @NDCG, @MRR.
#### `mf_simple.py` : 
Matrix Factorization model definition.
#### `MLP.py` :
Multi-Layer Perceptron model definition.

## References
[Microsoft News Data](https://msnews.github.io/)

[Google Research - Matrix Factoizaiton Experiment](https://github.com/google-research/google-research/tree/master/dot_vs_learned_similarity)

[Neural Collaborative Filtering Original Experiment](https://github.com/hexiangnan/neural_collaborative_filtering)
